const sequelize = require('../db')
const {DataTypes} = require('sequelize')

//initialize model for user in db and distinguish its properties
const User = sequelize.define( 'user', {
    //primary key - key for connection with other models
    //autoIncrement - increase value of every userid by 1, in order to avoid repetition
    id: {type: DataTypes.INTEGER, primaryKey: true, autoIncrement: true},
    //can't create accounts on same email
    email: {type: DataTypes.STRING, unique: true},
    name: {type: DataTypes.STRING},
    password: {type: DataTypes.STRING},
    role: {type: DataTypes.STRING, defaultValue: "USER"},
    courses_past: {type: DataTypes.ARRAY(DataTypes.INTEGER), defaultValue: []},
    courses_current: {type: DataTypes.ARRAY(DataTypes.INTEGER), defaultValue: []},
    photo: {type: DataTypes.STRING, defaultValue: "avatar.jpg"}
})

//initialize model for course in db and distinguish its properties
const Course = sequelize.define( 'course', {
    id: {type: DataTypes.INTEGER, primaryKey: true, autoIncrement: true},
    name: {type: DataTypes.STRING, unique: true, allowNull: false},
    costs: {type: DataTypes.ARRAY(DataTypes.INTEGER), allowNull: false},
    time_hours: {type: DataTypes.STRING, allowNull: false},
    time_days: {type: DataTypes.STRING, allowNull: false},
    duration: {type: DataTypes.STRING, allowNull: false},
    //what you learn in array of strings
    description: {type: DataTypes.ARRAY(DataTypes.STRING), allowNull: false},
    //store modules with array of its parts
    modules: {type: DataTypes.ARRAY(DataTypes.ARRAY(DataTypes.STRING)), allowNull: false},
    img: {type: DataTypes.STRING, allowNull: false},
})

User.hasMany(Course)

module.exports = {
    User,
    Course
}