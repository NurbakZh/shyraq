//combine routes in one file for easier exporting
const Router = require('express')
const router = new Router()
const userRouter = require('./UserRouter')
const courseRouter = require('./CoursesRouter')

//connect user and courses routes
router.use('/user', userRouter)
router.use('/courses', courseRouter)

module.exports = router