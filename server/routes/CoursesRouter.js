const Router = require('express')
const router = new Router()
const CourseController = require('../controllers/CourseController')

//post method for creation and for getting the courses/one course
router.post('/', CourseController.create)
router.get('/', CourseController.get)
router.get('/:id', CourseController.getOne)

module.exports = router