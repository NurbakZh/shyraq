const {Sequelize} = require('sequelize')

//инициализация sequelize объекта для будущего подключения к дб
//(по тем данным, которые указали в pgAdmin)
module.exports = new Sequelize(
    process.env.DB_NAME,
    process.env.DB_USER,
    process.env.DB_PASSWORD,
    {
        dialect: 'postgres',
        host: process.env.DB_HOST,
        port: process.env.DB_PORT
    },
)