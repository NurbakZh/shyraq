//имортируем созданный sequelize для подключения к дб и express для работы с этим дб
//dotenv - для получение перменных со среды(.env)
require('dotenv').config()
const express = require('express')
const cors = require('cors')
const sequelize = require('./db')
const models = require('./models/models')
const path = require('path')
const fileUpload = require('express-fileupload')
const router = require('./routes/index')
const PORT = process.env.PORT || 5001

const app = express()
app.use(cors())
app.use(express.json())
app.use(express.static(path.resolve(__dirname, 'static')))
app.use(fileUpload({}))
app.use('/api', router)
//функция ассинхронна, поскольку все операции с дб асинхронны
const start = async () => {
    try {
        //подключение к базе данных(await из-за асинхронности функции)
        await sequelize.authenticate();
        //сверяем сосотояние базы данных со схемой данных(которую опишу позже)
        await sequelize.sync();
        app.listen(PORT, () => console.log(`Server started on port ${PORT}`))
    } catch (e) {
        //отлавливаем ошибку
        console.log(e)
    }
}

start();




