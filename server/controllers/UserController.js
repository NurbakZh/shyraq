const ApiError = require('../errors/ApiError')
const {User, Basket} = require('../models/models')

class UserController {
    async registration(req, res, next) {
        try {
            //get data from request body
            const {email, name, password} = req.body
            //check if user with such an email already exists
            const candidate = await User.findOne({where: {email}})
            if(candidate) {
                return next(ApiError.badRequest("user with such email already exists"))
            }
            //create user if everything is ok
            const user = await User.create({email, name, password})
        } catch (e) {
            next(ApiError.badRequest(e.message))
        }
        return res.json("ok")
    }
    async login(req, res, next) {
        //get data from request body
        const {email, password} = req.body
        const user = await User.findOne({where: {email}})
        //check if user with such email exists
        if(!user) {
            return next(ApiError.internalError("no such user with this id"))
        }
        //check if password is correct
        if(password!==user.password) {
            return next(ApiError.internalError(`password for user with email: ${email} is incorrect`))
        }
        return res.json(user.name)
    }
    async check(req, res) {
        return res.json('fff')
    }
}

module.exports = new UserController()