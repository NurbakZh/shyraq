// Depending on the specific mechanisms used, a UUID is either guaranteed to be
// different or is, at least, extremely likely to be different from any other
// UUID generated until A.D. 3400. We used it to name photos of courses differently
const uuid = require('uuid')
const path = require('path')
const {Course} = require('../models/models')
const ApiError = require('../errors/ApiError')
const {DataTypes} = require("sequelize");

class CourseController {
    async create(req, res, next) {
        try {
            //get data from request bodyd
            let {name, costs, time_hours, time_days, duration,
                description, modules} = req.body
            const {img} = req.files
            //create unique filename img
            let fileName = uuid.v4() + ".png"
            //save it on the server
            img.mv(path.resolve(__dirname, '..', 'static', fileName))
            //convert arrays in form of string to array
            const costsArr = JSON.parse(costs);
            const descriptionArr = JSON.parse(description);
            const modulesArr = JSON.parse(modules);
            //create new course object
            const course = await Course.create({
                name, costs: costsArr, time_hours, time_days,
                duration, description: descriptionArr,
                modules: modulesArr, img: fileName})
            return res.json(course)
        } catch (e) {
            next(ApiError.badRequest(e.message))
        }
    }

    //get all courses
    async get(req, res) {
        let courses = await Course.findAndCountAll();
        return res.json(courses)
    }
    //get course by id
    async getOne(req, res) {
        const {id} = req.params
        const course = await Course.findOne({
            where: {id}
        })
        return res.json(course)
    }
}

module.exports = new CourseController()