import React, {useEffect, useState} from 'react';
import AppBar from '@mui/material/AppBar';
import Box from '@mui/material/Box';
import Toolbar from '@mui/material/Toolbar';
import IconButton from '@mui/material/IconButton';
import Typography from '@mui/material/Typography';
import Menu from '@mui/material/Menu';
import MenuIcon from '@mui/icons-material/Menu';
import Container from '@mui/material/Container';
import Button from '@mui/material/Button';
import MenuItem from '@mui/material/MenuItem';
import {useNavigate} from "react-router-dom";

interface Props {
    onCoursesScroll?: () => void;
    onAboutScroll?: () => void;
}

export const LoginHeader: React.FC<Props> = ({onCoursesScroll, onAboutScroll}) => {
    const [user, setUser] = useState('');
    const navigate = useNavigate();
    useEffect(() => {
        if (localStorage.getItem('user') !== null) {
            // @ts-ignore
            setUser(localStorage.getItem('user'));
        }
    }, [user, setUser]);
    const logOut = () => {
        localStorage.removeItem('user');
        navigate('/');
    }
    const pages = ['О школе', 'Курсы'];
    const [anchorElNav, setAnchorElNav] = React.useState<null | HTMLElement>(null);

    const handleOpenNavMenu = (event: React.MouseEvent<HTMLElement>) => {
        setAnchorElNav(event.currentTarget);
    };

    const handleCoursesNavMenu = () => {
        if (onCoursesScroll) {
            onCoursesScroll();
        }
        setAnchorElNav(null);
    };

    const handleAboutNavMenu = () => {
        // @ts-ignore
        onAboutScroll();
        setAnchorElNav(null);
    };

    return (
        <AppBar position="sticky" sx={{ backgroundColor: '#13171D' }}>
            <Container>
                <Toolbar disableGutters>
                    <Box sx={{ flexGrow: 1, display: { xs: 'flex', md: 'none' } }}>
                        <IconButton
                            size="large"
                            onClick={handleOpenNavMenu}
                            color="inherit"
                        >
                            <MenuIcon sx={{ color: '#7B879A' }} />
                        </IconButton>
                        <Menu
                            id="menu-appbar"
                            anchorEl={anchorElNav}
                            anchorOrigin={{
                                vertical: 'bottom',
                                horizontal: 'left',
                            }}
                            keepMounted
                            transformOrigin={{
                                vertical: 'top',
                                horizontal: 'left',
                            }}
                            open={Boolean(anchorElNav)}
                            onClose={() => setAnchorElNav(null)}
                            sx={{
                                '& .MuiPaper-root': {
                                    backgroundColor: '#13171D',
                                },
                                display: { xs: 'block', md: 'none' },
                            }}
                        >
                            {pages.map((page, index) => (
                                <MenuItem
                                    key={page}
                                    onClick={index === 0 ? handleAboutNavMenu : handleCoursesNavMenu}
                                    sx={{
                                        backgroundColor: '#13171D',
                                        "&:hover": {
                                            backgroundColor: '#0F1216'
                                        }
                                    }}>
                                    <Typography textAlign="center" sx={{
                                        color: '#7B879A',
                                        fontFamily: 'Igra Sans',
                                    }}>{page}</Typography>
                                </MenuItem>
                            ))}
                        </Menu>
                    </Box>
                    <Typography
                        variant="h5"
                        noWrap
                        component="a"
                        href=""
                        sx={{
                            mr: 2,
                            display: { xs: 'flex', md: 'none' },
                            flexGrow: 1,
                            color: '#7B879A',
                            fontFamily: 'Igra Sans',
                            fontWeight: 400,
                            letterSpacing: '.3rem',
                            textDecoration: 'none',
                        }}
                    >
                        SHYRAQ
                    </Typography>
                    <Box sx={{ flexGrow: 1, display: { xs: 'none', md: 'flex' } }}>
                        {pages.map((page, index) => (
                            <Button
                                key={page}
                                onClick={index === 0 ? handleAboutNavMenu : handleCoursesNavMenu}
                                sx={{
                                    my: 2,
                                    textTransform: 'none',
                                    color: '#7B879A',
                                    fontFamily: 'Igra Sans',
                                    display: 'block',
                                    fontSize: '20px',
                                }}
                            >
                                {page}
                            </Button>
                        ))}
                    </Box>
                    {user === '' && <Button
                        onClick={() => navigate('/login')}
                        sx={{
                            color: '#7B879A',
                            textTransform: 'none',
                            fontFamily: 'Igra Sans',
                            display: 'block',
                            fontSize: {xs: '16px', sm: '18px', md: '20px'},
                        }}>Вход</Button>
                    }
                    {user !== '' && <Button
                        onClick={logOut}
                        sx={{
                            color: '#7B879A',
                            textTransform: 'none',
                            fontFamily: 'Igra Sans',
                            display: 'block',
                            fontSize: {xs: '16px', sm: '18px', md: '20px'},
                        }}>{user}, нажмите чтобы выйти</Button>}
                </Toolbar>
            </Container>
        </AppBar>
    )
};