import React from 'react';
import {LoginHeader} from "./components/LoginHeader";
import {OtherHeader} from "./components/OtherHeader";

interface Props {
    goBack?: () => void;
    onCoursesScroll?: () => void;
    onAboutScroll?: () => void;
    type: 'main' | 'login' | 'courses';
}

export const Header: React.FC<Props> = ({onCoursesScroll, onAboutScroll, goBack, type}) => {
    if (type === 'main') {
        return <LoginHeader onCoursesScroll={onCoursesScroll} onAboutScroll={onAboutScroll} />
    } else {
        return <OtherHeader goBack={goBack} />
    }
};