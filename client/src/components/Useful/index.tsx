import {Box, Grid} from "@mui/material";
import money from '../../imgs/money.svg';
import book from '../../imgs/book.svg';
import bulb from '../../imgs/bulb.svg';
import phone from '../../imgs/phone.svg';
import Typography from "@mui/material/Typography";

export const Useful: React.FC = () => {
    return (
        <Box sx={{ p: { xs: '24px', sm: '36px', md: '72px' }}}>
            <Typography
                sx={{
                    color: 'white',
                    fontFamily: 'Igra Sans',
                    fontSize: {xs: '24px', sm: '36px', md: '48px'},
                    lineHeight: {xs: '36px', sm: '48px', md: '60px'},
                    letterSpacing: '0.01em',
                    mb: {xs: '12px', sm: 5 },
                }}
            >
                Мы вам подходим,
                если ...
            </Typography>
            <Grid container spacing={5}>
                <Grid item xs={12} md={4}>
                    <Box sx={{
                        height: '80%',
                        border: '1px solid rgba(255, 255, 255, 0.15)',
                        borderRadius: '16px',
                        padding: 3,
                        display: 'flex',
                        flexDirection: 'column',
                    }}>
                        <img src={bulb} alt={bulb} style={{
                            backgroundColor: '#C1D6FF',
                            borderRadius: 100,
                            height: '117px',
                            width: '107px',
                            marginBottom: '18px',
                        }} />
                        <Typography
                            sx={{
                                fontFamily: 'Igra Sans',
                                fontSize: '24px',
                                lineHeight: '32px',
                            }}
                        >
                            Вы ждете реальных результатов от обучения
                        </Typography>
                    </Box>
                </Grid>
                <Grid item xs={12} md={8}>
                    <Box sx={{
                        height: '80%',
                        border: '1px solid rgba(255, 255, 255, 0.15)',
                        borderRadius: '16px',
                        padding: 3,
                        display: 'flex',
                        flexDirection: 'column',
                    }}>
                        <img src={money} alt={money} style={{
                            backgroundColor: '#BCFFC7',
                            borderRadius: 100,
                            height: '117px',
                            width: '107px',
                            marginBottom: '18px',
                        }} />
                        <Typography
                            sx={{
                                fontFamily: 'Igra Sans',
                                fontSize: '24px',
                                lineHeight: '32px',
                            }}
                        >
                            Вы хотите, чтобы ребенок освоил навыки для актуальной профессии
                        </Typography>
                    </Box>
                </Grid>
                <Grid item xs={12} md={4}>
                    <Box sx={{
                        height: '80%',
                        border: '1px solid rgba(255, 255, 255, 0.15)',
                        borderRadius: '16px',
                        padding: 3,
                        display: 'flex',
                        flexDirection: 'column',
                    }}>
                        <img src={book} alt={book} style={{
                            backgroundColor: '#FEFFBA',
                            borderRadius: 100,
                            height: '117px',
                            width: '107px',
                            marginBottom: '18px',
                        }} />
                        <Typography
                            sx={{
                                fontFamily: 'Igra Sans',
                                fontSize: '24px',
                                lineHeight: '32px',
                            }}
                        >
                            Вы хотите, чтобы ребенок поступил в престижный ВУЗ
                        </Typography>
                    </Box>
                </Grid>
                <Grid item xs={12} md={4}>
                    <Box sx={{
                        height: '80%',
                        border: '1px solid rgba(255, 255, 255, 0.15)',
                        borderRadius: '16px',
                        padding: 3,
                        display: 'flex',
                        flexDirection: 'column',
                    }}>
                        <img src={phone} alt={phone} style={{
                            backgroundColor: '#BAF3FF',
                            borderRadius: 100,
                            height: '117px',
                            width: '107px',
                            marginBottom: '18px',
                        }} />
                        <Typography
                            sx={{
                                fontFamily: 'Igra Sans',
                                fontSize: '24px',
                                lineHeight: '32px',
                            }}
                        >
                            Ваш ребенок увлекается играми
                        </Typography>
                    </Box>
                </Grid>
                <Grid item xs={12} md={4}>
                    <Box sx={{
                        height: '80%',
                        border: '1px solid rgba(255, 255, 255, 0.15)',
                        borderRadius: '16px',
                        padding: 3,
                        display: 'flex',
                        flexDirection: 'column',
                    }}>
                        <Typography
                            sx={{
                                fontFamily: 'Igra Sans',
                                fontSize: '24px',
                                lineHeight: '32px',
                            }}
                        >
                           Готовы развиваться здесь и сейчас
                        </Typography>
                    </Box>
                </Grid>
            </Grid>
        </Box>
    )
}