import {Courses} from '../../http';
import Box from "@mui/material/Box";
import Typography from "@mui/material/Typography";
import {Grid} from "@mui/material";
import React from "react";
import {useNavigate} from "react-router-dom";

interface Props {
    data: Courses[] | undefined;
}

export const CoursesPart = React.forwardRef<HTMLElement, Props>(
    (
        { data }: Props,
        ref: React.ForwardedRef<HTMLElement>,
    ): JSX.Element => {
    const colorsArr = ['#BAF3FF', '#FEFFBA', '#BCFFC7', '#C1D6FF'];
    const navigate = useNavigate();
    return (
        <Box sx={{ p: { xs: '24px', sm: '36px', md: '72px' }}} ref={ref}>
            <Typography
                sx={{
                    color: 'white',
                    fontFamily: 'Igra Sans',
                    fontSize: {xs: '24px', sm: '36px', md: '48px'},
                    lineHeight: {xs: '36px', sm: '48px', md: '60px'},
                    letterSpacing: '0.01em',
                    mb: {xs: '12px', sm: 5 },
                }}
            >
                Выберите направление
            </Typography>
            <Grid container spacing={5}>
                {data !== undefined && data.map((data, index) => {
                  return (
                      <Grid item xs={12} md={6} key={index}>
                          <Box onClick={() => navigate(`/course/${index}`)} sx={{
                              height: '80%',
                              borderRadius: '20px',
                              padding: 3,
                              cursor: 'pointer',
                              display: 'flex',
                              flexDirection: 'column',
                              backgroundColor: colorsArr[Math.floor(Math.random() * 3)],
                          }}>
                              <Typography
                                  sx={{
                                      fontFamily: 'Igra Sans',
                                      fontSize: '28px',
                                      lineHeight: '32px',
                                      color: ' #1D2023',
                                  }}
                              >
                                  {data.name}
                              </Typography>
                              <Box sx={{
                                  display: 'flex',
                                  flexDirection: 'row',
                              }}>
                                  <Typography
                                      sx={{
                                          mt: 3,
                                          fontFamily: 'Igra Sans',
                                          fontSize: '20px',
                                          lineHeight: '24px',
                                          color: ' #1D2023',
                                          mb: 4,
                                      }}
                                  >
                                      {data.description[0]}
                                  </Typography>
                                  <img
                                      src={`http://localhost:5002/${data.img}`}
                                      alt={'img'}
                                      style={{
                                          borderRadius: '20px',
                                          width: '50%',
                                          objectFit: 'scale-down',
                                      }}
                                  />
                              </Box>
                          </Box>
                      </Grid>
                  )
                })}
            </Grid>
        </Box>
    );
},);