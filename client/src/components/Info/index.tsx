import Box from "@mui/material/Box";
import girl from '../../imgs/firstblock_girl.svg';

export const Info: React.FC = () => {
    return (
        <Box sx={{
            position: 'relative',
            backgroundColor: '#0F1216',
            borderRadius: '20px',
            width: 'calc(100%-72px)',
            margin: {xs: '12px', md: '36px 72px'},
            height: 'auto',
            display: 'flex',
            flexDirection: {xs: 'column', md: 'row'},
            justifyContent: 'space-between',
        }}>
            <Box sx={{
                zIndex: 1000,
                width: {xs: 'calc(100%-72px)', md: '60%'},
                justifyContent: 'center',
                alignItems: 'center',
                py: {xs: '24px', sm: '49px', md: '98px' },
                pl: {xs: '24px', sm: '36px', md: '72px' },
                pr: {xs: '24px', sm: '36px', md: '0' },
                fontFamily: 'Igra Sans'
            }}>
                <Box sx={{
                    backgroundColor: '#1B1E22',
                    borderRadius: '20px',
                    maxWidth: 'fit-content',
                    padding: {xs: '10px', sm: '12px', md: '16px' },
                    fontWeight: 400,
                    fontSize: {xs: '16px', sm: '24px', md: '36px'},
                    lineHeight: {xs: '16px', sm: '24px', md: '36px'},
                }}>SHYRAQ</Box>
                <Box sx={{
                    mt: {xs: 2, sm: 4, md: 5},
                    fontSize: {xs: '24px', sm: '48px', md: '72px'},
                    lineHeight: {xs: '32px', sm: '56px', md: '84px'},
                }}>Программируем успешное будущее</Box>
                <Box sx={{
                    mt: {xs: 2, sm: 4, md: 5},
                    fontSize: {xs: '16px', sm: '24px', md: '36px'},
                    lineHeight: {xs: '20px', sm: '32px', md: '44px'},
                }}>Школа программирования для детей 7-17 лет</Box>
            </Box>
            <Box sx={{
                position: 'absolute',
                bottom: '0',
                right: 0,
                display: {xs: 'none', md: 'block'}
            }}>
                <img src={girl} alt={girl} style={{
                    borderRadius: 20,
                    width: '40vw'
                }} />
            </Box>
        </Box>
    )
};