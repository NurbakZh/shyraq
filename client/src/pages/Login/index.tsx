import React from "react";
import {Header} from "../../components/Header";
import { useNavigate } from "react-router-dom";
import Box from "@mui/material/Box";
import {Log} from "./Log";

export const Login: React.FC = () => {
    const navigate = useNavigate();
    const goBack = () => navigate('/')

    return (
        <div style={{ width: '100%', height: '100%' }}>
            <Header type={'login'} goBack={goBack} />
            <Box sx={{ p: { xs: '24px', sm: '36px', md: '72px' }}}>
                <Log />
            </Box>
        </div>
    );
}