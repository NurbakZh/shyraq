import React, {useEffect, useState} from "react";
import {Header} from "../../components/Header";
import { useNavigate } from "react-router-dom";
import {Courses, getCourses} from "../../http";
import {Logo} from "./components/Logo";
import {Info} from "./components/Info";
import {DetailedInfo} from "./components/DetailedInfo";
import {Chrt} from "./components/Chrt";


export const CoursePage: React.FC = () => {
    const [data, setData] = useState<Courses[]>();
    const navigate = useNavigate();
    const params = window.location.pathname;
    const index = Number(params.split('/')[2]);
    useEffect(() => {
        if (data === undefined) {
            getCourses().then(r => {
                if (typeof r !== 'string') {
                    // @ts-ignore
                    setData(r.rows);
                } else {
                    alert('whoops something went wrong');
                }
            });
        }
    }, [data, setData]);
    const goBack = () => navigate('/')

    return (
        <div style={{ width: '100%', height: '100%' }}>
            <Header type={'login'} goBack={goBack} />
            <Logo data={data} index={index}/>
            <Info data={data} index={index}/>
            <DetailedInfo data={data} index={index}/>
            <Chrt />
        </div>
    );
}