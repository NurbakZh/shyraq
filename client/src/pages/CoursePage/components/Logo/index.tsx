import Box from "@mui/material/Box";
import {Courses} from "../../../../http";

interface Props {
    index: number;
    data: Courses[] | undefined;
}

export const Logo: React.FC<Props> = ({index, data}) => {
    const colorsArr = ['#BAF3FF', '#FEFFBA', '#BCFFC7', '#C1D6FF'];
    return (
        <Box sx={{
            position: 'relative',
            backgroundColor: colorsArr[Math.floor(Math.random() * 3)],
            borderRadius: '20px',
            width: 'calc(100%-72px)',
            margin: {xs: '12px', md: '36px 72px'},
            height: 'auto',
            display: 'flex',
            flexDirection: {xs: 'column', md: 'row'},
            justifyContent: 'space-between',
        }}>
            <Box sx={{
                zIndex: 1000,
                width: {xs: 'calc(100%-72px)', md: '60%'},
                justifyContent: 'center',
                alignItems: 'center',
                py: {xs: '24px', sm: '49px', md: '98px' },
                pl: {xs: '24px', sm: '36px', md: '72px' },
                pr: {xs: '24px', sm: '36px', md: '0' },
                fontFamily: 'Igra Sans'
            }}>
                <Box sx={{
                    color: '#1D2023',
                    fontSize: {xs: '16px', sm: '24px', md: '36px'},
                    lineHeight: {xs: '16px', sm: '24px', md: '36px'},
                }}>Shyraq</Box>
                <Box sx={{
                    color: '#1D2023',
                    mt: {xs: 2, sm: 4, md: 5},
                    fontSize: {xs: '24px', sm: '36px', md: '48px'},
                    lineHeight: {xs: '32px', sm: '46px', md: '52px'},
                }}>{data && data[index] && data[index].name}</Box>
                <Box sx={{
                    color: '#1D2023',
                    mt: {xs: 2, sm: 4, md: 5},
                    fontSize: {xs: '16px', sm: '18px', md: '24px'},
                    lineHeight: {xs: '20px', sm: '22px', md: '28px'},
                }}>Занятия проходят онлайн{' '}
                    {data && data[index] && data[index].time_days}
                    {' '}раза в неделю
                </Box>
                <Box sx={{
                    color: '#1D2023',
                    mt: 1,
                    fontSize: {xs: '16px', sm: '18px', md: '24px'},
                    lineHeight: {xs: '20px', sm: '22px', md: '28px'},
                }}>Длительность занятия{' '}
                    {data && data[index] && data[index].time_hours}{' '}мин
                </Box>
                <Box sx={{
                    color: '#1D2023',
                    mt: {xs: 2, sm: 4, md: 5},
                    fontSize: {xs: '16px', sm: '18px', md: '24px'},
                    lineHeight: {xs: '20px', sm: '22px', md: '28px'},
                }}><span style={{ color: '#7B879A' }}>В группе (до 8 человек)</span>{' '}
                    {data && data[index] && data[index].costs[0]}{' '}тг/мес
                </Box>
                <Box sx={{
                    color: '#1D2023',
                    mt: 1,
                    fontSize: {xs: '16px', sm: '18px', md: '24px'},
                    lineHeight: {xs: '20px', sm: '22px', md: '28px'},
                }}><span style={{ color: '#7B879A' }}>Индивидуально</span>{' '}
                    {data && data[index] && data[index].costs[1]}{' '}тг/мин
                </Box>
            </Box>
            <Box sx={{
                position: 'absolute',
                bottom: '0',
                right: 0,
                display: {xs: 'none', md: 'block'}
            }}>
                <img src={data && data[index] && `http://localhost:5002/${data[index].img}`} alt={'img'} style={{
                    borderRadius: 20,
                    width: '40vw'
                }} />
            </Box>
        </Box>
    )
};