import Box from "@mui/material/Box";
import {Courses} from "../../../../http";
import Typography from "@mui/material/Typography";
import {Grid} from "@mui/material";
import React from "react";
import planet from '../../../../imgs/planet.svg';
import human from '../../../../imgs/human.svg';
import time from '../../../../imgs/time.svg';
import calendar from '../../../../imgs/calendar.svg';

interface Props {
    index: number;
    data: Courses[] | undefined;
}

export const DetailedInfo: React.FC<Props> = ({index, data}) => {
    return (
        <Box sx={{ p: { xs: '24px', sm: '36px', md: '72px' }}}>
            <Typography
                sx={{
                    color: 'white',
                    fontFamily: 'Igra Sans',
                    fontSize: {xs: '24px', sm: '36px', md: '48px'},
                    lineHeight: {xs: '36px', sm: '48px', md: '60px'},
                    letterSpacing: '0.01em',
                    mb: {xs: '12px', sm: 5 },
                }}
            >
                Как проходят занятия?
            </Typography>
            <Grid container spacing={5}>
                <Grid item xs={12} md={6} key={index}>
                    <Box sx={{
                        height: '70%',
                        borderRadius: '20px',
                        padding: 3,
                        cursor: 'pointer',
                        display: 'flex',
                        flexDirection: 'column',
                        border: '1px solid rgba(255, 255, 255, 0.15)',
                        backgroundColor: '#13171D',
                    }}>
                        <Box sx={{
                            display: 'flex',
                            flexDirection: 'row',
                        }}>
                            <Box>
                                <Typography
                                    sx={{
                                        fontFamily: 'Igra Sans',
                                        fontSize: '28px',
                                        lineHeight: '32px',
                                    }}
                                >
                                    {data && data[index].duration}{' '} месяца обучения
                                </Typography>
                                <Typography
                                    sx={{
                                        mt: 1,
                                        fontFamily: 'Igra Sans',
                                        fontSize: '20px',
                                        lineHeight: '24px',
                                    }}
                                >
                                    За это время ученики освоят необходимые навыки и соберут в портфолио от 3х работ
                                </Typography>
                            </Box>
                            <img src={calendar} alt={calendar} />
                        </Box>
                    </Box>
                </Grid>
                <Grid item xs={12} md={6} key={index}>
                    <Box sx={{
                        height: '70%',
                        borderRadius: '20px',
                        padding: 3,
                        cursor: 'pointer',
                        display: 'flex',
                        flexDirection: 'column',
                        border: '1px solid rgba(255, 255, 255, 0.15)',
                        backgroundColor: '#13171D',
                    }}>
                        <Box sx={{
                            display: 'flex',
                            flexDirection: 'row',
                        }}>
                            <Box>
                                <Typography
                                    sx={{
                                        fontFamily: 'Igra Sans',
                                        fontSize: '28px',
                                        lineHeight: '32px',
                                    }}
                                >
                                    {data && data[index].time_hours}{' '} мин
                                </Typography>
                                <Typography
                                    sx={{
                                        mt: 1,
                                        fontFamily: 'Igra Sans',
                                        fontSize: '20px',
                                        lineHeight: '24px',
                                    }}
                                >
                                    Длительность одного занятия. Этого хватает, чтобы освоить теорию и попрактиковаться
                                </Typography>
                            </Box>
                            <img src={time} alt={time} />
                        </Box>
                    </Box>
                </Grid>
                <Grid item xs={12} md={6} key={index}>
                    <Box sx={{
                        height: '70%',
                        borderRadius: '20px',
                        padding: 3,
                        cursor: 'pointer',
                        display: 'flex',
                        flexDirection: 'column',
                        border: '1px solid rgba(255, 255, 255, 0.15)',
                        backgroundColor: '#13171D',
                    }}>
                        <Box sx={{
                            display: 'flex',
                            flexDirection: 'row',
                        }}>
                            <Box>
                                <Typography
                                    sx={{
                                        fontFamily: 'Igra Sans',
                                        fontSize: '28px',
                                        lineHeight: '32px',
                                    }}
                                >
                                    Онлайн,{' '}{data && data[index].time_days}{' '}раза в неделю
                                </Typography>
                                <Typography
                                    sx={{
                                        mt: 1,
                                        fontFamily: 'Igra Sans',
                                        fontSize: '20px',
                                        lineHeight: '24px',
                                    }}
                                >
                                    Расписание зависит от группы, мы сделаем
                                    все возможное, чтобы вам было удобно
                                </Typography>
                            </Box>
                            <img src={planet} alt={planet} />
                        </Box>
                    </Box>
                </Grid>
                <Grid item xs={12} md={6} key={index}>
                    <Box sx={{
                        height: '70%',
                        borderRadius: '20px',
                        padding: 3,
                        cursor: 'pointer',
                        display: 'flex',
                        flexDirection: 'column',
                        border: '1px solid rgba(255, 255, 255, 0.15)',
                        backgroundColor: '#13171D',
                    }}>
                        <Box sx={{
                            display: 'flex',
                            flexDirection: 'row',
                        }}>
                            <Box>
                                <Typography
                                    sx={{
                                        fontFamily: 'Igra Sans',
                                        fontSize: '28px',
                                        lineHeight: '32px',
                                    }}
                                >
                                    В группе до 8 детей
                                </Typography>
                                <Typography
                                    sx={{
                                        mt: 1,
                                        fontFamily: 'Igra Sans',
                                        fontSize: '20px',
                                        lineHeight: '24px',
                                    }}
                                >
                                    Преподаватель успевает уделить внимание каждому ученику
                                </Typography>
                            </Box>
                            <img src={human} alt={human} />
                        </Box>
                    </Box>
                </Grid>
            </Grid>
        </Box>
    )
};