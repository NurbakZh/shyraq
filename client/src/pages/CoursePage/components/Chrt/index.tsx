import React from 'react';
import { LineChart, Line, XAxis, YAxis, CartesianGrid, Tooltip, Legend, ResponsiveContainer } from 'recharts';
import Typography from "@mui/material/Typography";
import Box from "@mui/material/Box";

const data = [
    {
        name: 'Неделя 1',
        our: 0,
        their: 0,
    },
    {
        name: 'Неделя 2',
        our: 20,
        their: 6,
    },
    {
        name: 'Неделя 3',
        our: 40,
        their: 10,
    },
    {
        name: 'Неделя 4',
        our: 60,
        their: 20,
    },
    {
        name: 'Неделя 5',
        our: 80,
        their: 8,
    },
    {
        name: 'Неделя 6',
        our: 100,
        their: 23,
    },
];

export const Chrt:React.FC = () => {
    return (
        <Box sx={{ p: { xs: '24px', sm: '36px', md: '72px' }, minHeight: '400px'}}>
            <Typography
                sx={{
                    color: 'white',
                    fontFamily: 'Igra Sans',
                    fontSize: {xs: '24px', sm: '36px', md: '48px'},
                    lineHeight: {xs: '36px', sm: '48px', md: '60px'},
                    letterSpacing: '0.01em',
                    mb: {xs: '12px', sm: 5 },
                }}
            >
                Какой тебя ожидает прогресс?
            </Typography>
            <ResponsiveContainer width="100%" height={400}>
                <LineChart
                    data={data}
                    margin={{
                        top: 5,
                        bottom: 5,
                    }}
                >
                    <CartesianGrid strokeDasharray="3 3" />
                    <XAxis dataKey="name" />
                    <YAxis />
                    <Tooltip />
                    <Legend />
                    <Line type="monotone" name="Наш студент" dataKey="our" stroke="#8884d8" activeDot={{ r: 8 }} />
                    <Line type="monotone" name="Обычный студент" dataKey="their" stroke="#82ca9d" />
                </LineChart>
            </ResponsiveContainer>
        </Box>
    );
}
