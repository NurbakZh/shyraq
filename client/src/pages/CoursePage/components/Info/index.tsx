import Box from "@mui/material/Box";
import {Courses} from "../../../../http";
import Typography from "@mui/material/Typography";
import {Grid} from "@mui/material";
import React from "react";

interface Props {
    index: number;
    data: Courses[] | undefined;
}

export const Info: React.FC<Props> = ({index, data}) => {
    return (
        <Box sx={{ p: { xs: '24px', sm: '36px', md: '72px' }}}>
            <Typography
                sx={{
                    color: 'white',
                    fontFamily: 'Igra Sans',
                    fontSize: {xs: '24px', sm: '36px', md: '48px'},
                    lineHeight: {xs: '36px', sm: '48px', md: '60px'},
                    letterSpacing: '0.01em',
                    mb: {xs: '12px', sm: 5 },
                }}
            >
                Что я получу от курса?
            </Typography>
            <Grid container spacing={5}>
                {data !== undefined && data[index].description.map((description, index) => {
                    return (
                        <Grid item xs={12} md={6} key={index}>
                            <Box sx={{
                                height: '70%',
                                borderRadius: '20px',
                                padding: 3,
                                cursor: 'pointer',
                                display: 'flex',
                                flexDirection: 'column',
                                backgroundColor: 'rgba(255, 255, 255, 0.1)',
                            }}>
                                <Box>
                                    <Typography
                                        sx={{
                                            fontFamily: 'Igra Sans',
                                            fontSize: '20px',
                                            lineHeight: '24px',
                                        }}
                                    >
                                        {description}
                                    </Typography>
                                </Box>
                            </Box>
                        </Grid>
                    )
                })}
            </Grid>
        </Box>
    )
};