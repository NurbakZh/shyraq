import React, {useEffect, useRef, useState} from "react";
import {Courses, getCourses} from "../../http";
import {Header} from "../../components/Header";
import {Info} from "../../components/Info";
import {Useful} from "../../components/Useful";
import {CoursesPart} from "../../components/CoursesPart";

export const Main: React.FC = () => {
    const [data, setData] = useState<Courses[]>();
    const coursesRef = useRef(null);
    const aboutRef = useRef(null);
    // @ts-ignore
    const handleCoursesScroll = () => coursesRef.current.scrollIntoView({ behavior: 'smooth', block: 'center' });
    // @ts-ignore
    const handleAboutScroll = () => aboutRef.current.scrollIntoView({ behavior: 'smooth', block: 'start' });

    useEffect(() => {
        if (data === undefined) {
            getCourses().then(r => {
                if (typeof r !== 'string') {
                    // @ts-ignore
                    setData(r.rows);
                } else {
                    alert('whoops something went wrong');
                }
            });
        }
    }, [data, setData]);

    return (
        <div style={{ width: '100%', height: '100%' }} ref={aboutRef}>
            <Header type={'main'} onCoursesScroll={handleCoursesScroll} onAboutScroll={handleAboutScroll} />
            <Info />
            <Useful />
            <CoursesPart ref={coursesRef} data={data} />
        </div>
    );
}